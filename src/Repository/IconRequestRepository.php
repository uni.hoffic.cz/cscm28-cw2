<?php
declare(strict_types=1);


namespace App\Repository;


use App\Controller\IndexController;
use App\Entity\IconRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class IconRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IconRequest::class);
    }

    public function registerHit(string $user, int $bit)
    {
        $this->getEntityManager()->persist(new IconRequest($bit, $user));
        $this->getEntityManager()->flush();
    }

    public function readBits($user): string
    {
        /** @var IconRequest[] $hits */
        $hits = $this->findBy(['X_user' => $user]);
        $bits = array_fill(0, IndexController::BIT_LENGTH, 1);

        foreach ($hits as $hit) {
            $bits[$hit->getBit()] = 0;
        }

        return implode('', $bits);
    }
}
