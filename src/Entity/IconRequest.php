<?php
declare(strict_types=1);


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class IconRequests
 * @package App\Entity
 * @ORM\Entity()
 */
class IconRequest
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $bit;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $X_user;

    /**
     * IconRequests constructor.
     * @param int $bit
     * @param string $user
     */
    public function __construct(int $bit, string $user)
    {
        $this->bit = $bit;
        $this->X_user = $user;
    }

    /**
     * @return int
     */
    public function getBit(): int
    {
        return $this->bit;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->X_user;
    }
}
