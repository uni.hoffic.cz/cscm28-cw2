<?php
declare(strict_types=1);


namespace App\Controller;


use App\Repository\IconRequestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    const BIT_LENGTH = 8;
    const SALT = 'NaCl';

    /**
     * @Route(path="/", name="index")
     * @param SessionInterface $session
     * @return Response
     */
    public function index(SessionInterface $session)
    {
        $key = '';
        for ($i = 0; $i < self::BIT_LENGTH; $i++) {
            $key .= rand(0, 1);
        }

        $session->set('Xid', rand());

        return $this->render('index.html.twig', [
            'read' => $this->isReadMode(),
            'key' => $key,
            'salt' => self::SALT,
        ]);
    }

    /**
     * @Route(path="/redirection/{key}/{bit}/", name="redirection")
     * @param string $key
     * @param int $bit
     * @return Response
     */
    public function redirection(string $key, int $bit)
    {
        return $this->render('redirection.html.twig', [
            'read' => $this->isReadMode(),
            'key' => $key,
            'bit' => $bit,
            'final' => $bit + 1 >= self::BIT_LENGTH,
            'salt' => self::SALT,
        ]);
    }

    /**
     * @Route(path="/finished-writing/", name="finished-writing")
     */
    public function finishedWriting()
    {
        return $this->render('finished_writing.html.twig', [
            'read' => $this->isReadMode(),
        ]);
    }

    /**
     * @Route(path="/finished-reading/", name="finished-reading")
     * @param SessionInterface $session
     * @param IconRequestRepository $iconRequestRepository
     * @return Response
     */
    public function finishedReading(SessionInterface $session, IconRequestRepository $iconRequestRepository)
    {
        return $this->render('finished_reading.html.twig', [
            'bits' => $iconRequestRepository->readBits(strval($session->get('Xid'))),
        ]);
    }

    /**
     * @Route(path="/icon/{salt}/{bit}/favicon.png", name="icon")
     * @param string $salt
     * @param int $bit
     * @param SessionInterface $session
     * @param IconRequestRepository $iconRequestRepository
     * @return Response
     */
    public function icon(string $salt, int $bit, SessionInterface $session, IconRequestRepository $iconRequestRepository)
    {
        if ($this->isReadMode()) {
            $iconRequestRepository->registerHit(
                strval($session->get('Xid')),
                $bit);

            return new Response(null, Response::HTTP_NOT_FOUND);

        } else {
            return new Response(
                readfile('../assets/static/favicon.png'),
                Response::HTTP_OK,
                [
                    'Content-Type' => 'image/png',
                ]
            );
        }
    }

    private function isReadMode()
    {
        return trim(file_get_contents('mode.txt')) === 'read';
    }
}
